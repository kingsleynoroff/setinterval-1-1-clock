# setInterval clock

Create a block that displays the current time in the format hours:minutes:seconds e.g. 12:34:56.  The time updates every second to look like a digital clock.

**Note:** Styles are provided for you in master.css. Look in the file to find the approriate class names to use on your HTML elements.

### Example

[The example is here](https://setinterval-1-1-clock.now.sh).

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Create a clock elememt in HTML.
1. Select the element in JavaScript.
1. Declare a function called updateClock.
    1. Inside the function create new Date object and store it in a variable
       
       To create a date object the code is `new Date()`.
    1. The date object can be used to get the current time and date.
       Using the object to get the current hour, minute and seconds.
       The object has a method for each of these.
    1. Join the hours minutes and seconds together to make the finished time
    1. Set the time into the clock element.
    1. Fixing single digit segments - sometimes the time will show something like 9:3:4 instead of 09:03:04.
       To fix this. Add an if statement for each part of the time, if the time is less than 10, add a 0 onto the start of it.
1. Call the updateClock function.  You should see the time each time the page loads, but it won't update unless you refresh the page.
1. Call setInterval and pass in the function and set the interval to 1000 milliseconds.