// select element from the DOM
const clock = document.getElementById("clock");

// call update clock function when the page loads, so that it doesnt take a second before the clock is displayed
updateClock();
// call setInterval and pass in a callback function and a time interval (in milliseconds)
// the callback function will be called repeatly every X milliseconds (X being the number of milliseconds passed to the function)
// we want our clock to update every second (1000 milliseconds)
setInterval(updateClock, 1000);

// create callback function
function updateClock() {
    // this code will run every second

    // create new date object and store it in a variable
    const date = new Date();
    // use the date object to get hours, minutes and seconds and store these in separate variables
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();

    // fix single digit times e.g. 09:04:03 would display as 9:4:3, 
    // so 0 needs to be added to each segment if the number is less than 10
    if(hours < 10) {
        hours = "0" + hours;
    }
    if(minutes < 10) {
        minutes = "0" + minutes;
    }
    if(seconds < 10) {
        seconds = "0" + seconds;
    }

    // join segments together and output into the clock element
    clock.innerText = hours + ":" + minutes + ":" + seconds;
}